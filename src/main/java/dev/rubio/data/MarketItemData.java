//where we will get the data from the database
package dev.rubio.data;

import dev.rubio.controller.ItemController;
import dev.rubio.models.MarketItem;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class MarketItemData {

    private List<MarketItem> items = new ArrayList<>();

    public MarketItemData(){
        super();
        items.add(new MarketItem(432,3.00, "Bread"));
        items.add(new MarketItem(435,2.00, "Bananas"));
        items.add(new MarketItem(438,7.00, "Cheese"));
    }

    public List<MarketItem> getAllItems(){
        return new ArrayList<>(items);
    }

    public MarketItem getItemById(int id){
        /*
        for(MarketItem item : items){
            if(id==item.getId()){
                return item;
            }
        }
        return null;
         */
        return items.stream().filter(item->item.getId()==id).findAny().orElse(null);
    }

    public MarketItem addNewItem(MarketItem item){
        items.add(item);
        return item;
    }

    public void deleteItem(int id){
//        items.removeIf(marketItem -> (marketItem!=null)?id==marketItem.getId():false);
        Predicate<MarketItem> idCheck = marketItem -> marketItem!=null && id==marketItem.getId();
        items.removeIf(idCheck);
        // this remove operation can also be handled using iteration, looping through and using the
        // list remove method
    }


}


package dev.rubio;
import dev.rubio.controller.ItemController;
import dev.rubio.data.MarketItemData;
import dev.rubio.models.MarketItem;
import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;

public class Driver {
    public static void main(String[] args) {
        Javalin app = Javalin.create().start();
        app.get("/",(Context ctx) -> {ctx.result("Hello World");});
//        app.get("/", ctx -> ctx.result("Hello World"));

        ItemController itemController = new ItemController();
        app.get("/items", itemController::handleGetItemsRequest);

        // The second parameter method reference is the ::
        app.get("/items/:id", itemController::handleGetItemByIdRequest);

        app.post("/items", itemController::handlePostNewItem );

        app.delete("/items", itemController::handleDeleteById);
    }

}
